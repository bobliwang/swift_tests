//
//  File.swift
//  calculator
//
//  Created by Li Wang on 24/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation

protocol EntityProtocol {
    
    init()
    
    var entityMetadata : EntityMetadata { get }
    
    func property(name : String, val : String)
}

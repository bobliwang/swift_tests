//
//  ColumnMetadata.swift
//  calculator
//
//  Created by Li Wang on 24/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation




class ColumnMetadata {
    
    var columnName : String = ""
    
    var propertyName : String = ""
    
    var sqlDataType : String = ""
    
    var isPrimaryKey : Bool = false
    
    var isAutoIncremental : Bool = false
    
    init(){
    }
}
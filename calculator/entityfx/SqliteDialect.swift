//
//  SqliteDialect.swift
//  calculator
//
//  Created by Li Wang on 24/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation

var sqliteDialect : SqliteDialect = SqliteDialect()

class SqliteDialect : SqlDialectProtocol {
    
    init(){
        
    }
    
    var id : String { get {
        return "Sqlite"
    }}
    
    var openQuote : String { get {
        return "'"
    }}
    
    var closeQuote : String { get {
        return "'"
    }}

}
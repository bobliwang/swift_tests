//
//  EntityManager.swift
//  calculator
//
//  Created by Li Wang on 24/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation


class EntityManager {
    
    var db : FMDatabase
    
    var alwaysKeepOpen : Bool = false
    
    var sqlDialect : SqlDialectProtocol = sqliteDialect
    
    
    init(db : FMDatabase){
        
        println("EntityManager.init(db : FMDatabase)")
        
        self.db = db
    }
    
    init(db: FMDatabase, alwaysKeepOpen : Bool){
        self.db = db
        self.alwaysKeepOpen = alwaysKeepOpen
        
        if(self.alwaysKeepOpen){
            self.db.open()
        }
    }

    
    var isConnected : Bool {
        get { return db.goodConnection() }
    }
    
    func performAction(action : (db : FMDatabase) -> Int32?) -> Int32? {
        
        println("performAction");
        
        var rowsAffected : Int32? = nil
        
        if(!self.isConnected){
            if(!self.db.open()){
                return rowsAffected;
            }
        }
        
        rowsAffected = action(db: self.db)
        
        
        if(!self.alwaysKeepOpen){
            self.db.close()
        }
        
        return rowsAffected
    }
    
    func performQuery<T>(qry : (db: FMDatabase) -> T?) -> T? {
        var t : T? = nil
        
        if(!self.isConnected){
            if(!self.db.open()){
                return t;
            }
        }
        
        t = qry(db: self.db)
        
        if(!self.alwaysKeepOpen){
            self.db.close()
        }
        
        return t;
    }
    
    func getAll<TEntity where TEntity : EntityProtocol>(sample : TEntity) -> Array<TEntity>? {
        var entityMetadata = sample.entityMetadata
        var sql = entityMetadata.getSelectSql(self.sqlDialect)
        
        var array = self.performQuery({(db : FMDatabase) -> Array<TEntity>? in
            
            var arr :  Array<TEntity> = []
            
            println(sql)
            
            var rs = db.executeQuery(sql, withArgumentsInArray: [])
            
            
            while rs != nil && rs.next() {
                
                var entity = TEntity()
                
                self.populateEntity(entity, resultset: rs)
                
                arr.append(entity)
            }
            
            
            return arr
        })
        
        return array
    }
    
    
    func populateEntity<TEntity where TEntity : EntityProtocol>(entity : TEntity, resultset : FMResultSet){
        
        var metadata = entity.entityMetadata;
        
        for col in metadata.columns.values {
            
            var val : String = resultset.stringForColumn(col.columnName)
            
            
            
            entity.property(col.propertyName, val: val)
        }
    }

    
}

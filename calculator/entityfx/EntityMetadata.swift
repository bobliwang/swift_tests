//
//  EntityMetadata.swift
//  calculator
//
//  Created by Li Wang on 24/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation




class EntityMetadata {
    
    var tableName : String?
    
    var columns = Dictionary<String, ColumnMetadata>()
    
    var cachedSqlsStorage = Dictionary<String, CachedSqls>()
    
    init(){
        
    }
    
    var primaryKeys : ColumnMetadata[] {
    get{
        
        var keys : ColumnMetadata[] = []
        
        for col in columns.values {
            keys.append(col)
        }
        
        return keys
    }
    }
    
    func addColumnMetadata(col:ColumnMetadata) -> EntityMetadata {
        
        columns[col.columnName] = col
        
        return self
    }
    
    func getSelectSql(dialect : SqlDialectProtocol) -> String {
        
        var cachedSqls : CachedSqls? = self.cachedSqlsStorage[dialect.id]
        
        if(cachedSqls == nil){
            cachedSqls = CachedSqls()
            self.cachedSqlsStorage[dialect.id] = cachedSqls
        }
        
        if(cachedSqls!.select == nil){
            cachedSqls!.select = self.createSelectSql(dialect)
        }
        
        return cachedSqls!.select!
    }
    
    func createSelectSql(dialect : SqlDialectProtocol) -> String {
        var sql = "SELECT "
        
        for col in self.columns.values{
            sql += col.columnName + ", "
        }
        
        sql = sql.substringToIndex(countElements(sql) - 2)
        
        
        sql += " FROM \(dialect.openQuote)\(self.tableName!)\(dialect.closeQuote)"
        
        return sql
    }
    
}


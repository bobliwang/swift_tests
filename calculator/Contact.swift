//
//  Contact.swift
//  calculator
//
//  Created by Li Wang on 22/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation


var contactEntityMetadata = EntityMetadata()


class Contact : EntityProtocol {
    
    init(){
        
    }
    
    init(firstname:String, lastname:String){
        self.firstname = firstname
        self.lastname = lastname
    }
    
    var firstname : String = ""
    
    var lastname : String = ""
    
    
    var entityMetadata : EntityMetadata {
        get{
            if(contactEntityMetadata.columns.count == 0){
                
                contactEntityMetadata.tableName = "Contacts"
                
                var firstnameCol = ColumnMetadata()
                firstnameCol.columnName = "Firstname"
                firstnameCol.propertyName = "firstname"
                
                contactEntityMetadata.addColumnMetadata(firstnameCol)
                
                var lastnameCol = ColumnMetadata()
                lastnameCol.columnName = "Lastname"
                lastnameCol.propertyName = "lastname"
                
                contactEntityMetadata.addColumnMetadata(lastnameCol)

            }
            
            return contactEntityMetadata
        }
    }
    
    func property(name: String, val: String) {
    
        
        switch name {
        case "firstname":
            self.firstname = val
        case "lastname" :
            self.lastname = val
        default:
            println("Invalid property name: \(name)")
        }
        
    }
    
    
    
    var fullname : String {
    
        get{
            return firstname + " " + lastname
        }
    }
    
}
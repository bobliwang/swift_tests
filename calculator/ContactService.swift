//
//  ContactService.swift
//  calculator
//
//  Created by Li Wang on 22/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation


protocol ContactServiceProtocal
{
    func getContacts() -> Array<Contact>
    
    func addContact(contact : Contact)
    
    
    
}


class ContactService : ContactServiceProtocal{
    
    var db : FMDatabase
    
    var entityManager : EntityManager
    
    init(db:FMDatabase, entityManager: EntityManager){
        
        
        self.db = db
        
        
        println("ContactService init 0 ... ")
        println("ContactService init 1 .. ")
        
        self.entityManager = entityManager
        
        println("ContactService init 2 .. ")
        
        println("db created")
        
        self.entityManager.performAction({db in
            var sql = "Create Table IF NOT EXISTS Contacts (Firstname NVARCHAR(200), Lastname NVARCHAR(200))"
            
            var result1 = db.executeUpdate(sql, withArgumentsInArray: [])
            
            println("result1:\(result1)")
            
            
            var result3 = db.executeUpdate("DELETE FROM Contacts", withArgumentsInArray: [])
            println("result3:\(result3)")
            
            return db.changes()
        })
    }
    
    
    func getContacts() -> Array<Contact> {
        var array = self.entityManager.getAll(Contact())
        
        if(array == nil){
            return []
        }
        
        return array!
    }
    
    func addContact(contact : Contact) {
        
        self.entityManager.performAction({db in
            var result2 = db.executeUpdate("INSERT INTO Contacts VALUES ('\(contact.firstname)', '\(contact.lastname)')", withArgumentsInArray: [])
            
            println("result2:\(result2)")
            
            return db.changes()
        })
    }

}
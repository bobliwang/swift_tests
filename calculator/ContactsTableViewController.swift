//
//  ContactsTableViewController.swift
//  calculator
//
//  Created by Li Wang on 6/07/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import Foundation
import UIKit

class ContactsTableViewController : UITableViewController, UITableViewDelegate, UITableViewDataSource {
    var service : ContactServiceProtocal?
    
//    init(style: UITableViewStyle){
//        
//        super.init(style: style)
//        
//    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func getContactService() -> ContactServiceProtocal {
        
        if(self.service == nil){
            var db = FMDatabase(path: "/tmp/temp.db")
            var em = EntityManager(db: db)
            self.service = ContactService(db: db, entityManager: em)
        }
        
        return self.service!
    }
    
    
    override func loadView (){
        
        
        var tableView = UITableView(frame: UIScreen.mainScreen().applicationFrame, style: UITableViewStyle.Plain)
        
        tableView.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.reloadData();
        
        self.view = tableView;
    }
    
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    func getContacts() -> Array<Contact> {
        return getContactService().getContacts()
    }
    
    override func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return self.getContacts().count
    }
    
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let identifier = "MyTestCell"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? UITableViewCell
        
        
        
        if(cell == nil){
            cell  = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: identifier)
            
            //println("creating")
            
        }
        else{
            
            
            //println("reusing")
        }
        
        var contacts = self.getContacts()
        
        var contact = contacts[indexPath.row]
        
        cell.text = contact.firstname
        cell.detailTextLabel.text = "Fullname: #\(contact.fullname)"
        
        return cell
    }


}
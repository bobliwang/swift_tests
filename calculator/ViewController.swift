//
//  ViewController.swift
//  calculator
//
//  Created by Li Wang on 18/06/2014.
//  Copyright (c) 2014 Li Wang. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
                            
    @IBOutlet var button : UIButton
    
    @IBOutlet var tableView : UITableView
    
    var i : Int = 0
    
    var service : ContactServiceProtocal?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    @IBAction func buttonClicked(sender : AnyObject) {

        if(service == nil){
            var db = FMDatabase(path: "/tmp/temp.db")
            var em = EntityManager(db: db)
            self.service = ContactService(db: db, entityManager: em)
            
        }
        
        addNewContact()
        
        tableView.reloadData()
        
        
        
    }
    
    func addNewContact(){
        
        
        var contact = Contact(firstname: "bob", lastname: "wang \(i++)")
        
        service!.addContact(contact)
        
    }
    
    
    
    
}



// for UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int {
        return 1
    }
    
    func getContacts() -> Array<Contact> {
        if(service == nil){
            return []
        }
        
        return service!.getContacts()
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return self.getContacts().count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let identifier = "MyTestCell"
        
        var cell : UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? UITableViewCell
        
        
        
        if(cell == nil){
           cell  = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: identifier)
            
            //println("creating")

        }
        else{
            
            
            //println("reusing")
        }
        
        var contacts = self.getContacts()
        
        var contact = contacts[indexPath.row]
        
        cell.text = contact.firstname
        cell.detailTextLabel.text = "Fullname: #\(contact.fullname)"
        
        return cell
    }
}
